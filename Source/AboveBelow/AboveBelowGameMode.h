// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameMode.h"
#include "AboveBelowGameMode.generated.h"

/**
 * 
 */
UCLASS()
class ABOVEBELOW_API AAboveBelowGameMode : public AGameMode
{
	GENERATED_BODY()
	AAboveBelowGameMode(const FObjectInitializer& ObjectInitializer);
	virtual void StartPlay() override;
	
public:
	AAboveBelowGameMode();

};
