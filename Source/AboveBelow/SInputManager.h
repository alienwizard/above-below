// Fill out your copyright notice in the Description page of Project Settings.

#pragma once


#include "Widgets/SCompoundWidget.h"

/**
 * 
 */
class ABOVEBELOW_API SInputManager : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SInputManager)
	{}
	SLATE_END_ARGS()

	/** Constructs this widget with InArgs */
	void Construct(const FArguments& InArgs);
};
