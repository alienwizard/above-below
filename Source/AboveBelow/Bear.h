// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Pawn.h"
#include "Bear.generated.h"

UCLASS()
class ABOVEBELOW_API ABear : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ABear();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;


	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	virtual void changeColor();

	virtual void changePosition();

	UPROPERTY(Category = StaticMeshActor, VisibleAnywhere, BlueprintReadOnly,
		Meta = (ExposeFunctionCategories = "Mesh,Rendering,Physics,Components|StaticMesh", AllowPrivateAccess = "true"))
		UStaticMeshComponent * StaticMeshComponent;
	
	
};
