// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/PlayerController.h"
#include "MyPlayerController.generated.h"

class AAbovebelowCameraComponent;

/**
 * 
 */
UCLASS()
class ABOVEBELOW_API AMyPlayerController : public APlayerController
{
	GENERATED_UCLASS_BODY()
	
public:
	//begin PlayerController interface

protected:
	//

public:
	/*Set desired camera position*/
	void SetCameraTarget(const FVector& CameraTarget);

	/* helper function to toggle input detection*/
	void SetIgnoreInput(bool bIgnore);

	/*Input handlers*/
	void OnTapPressed(const FVector2D& ScreenPosition, float DownTime);
	void OnHoldPressed(const FVector2D& Screenposition, float Downtime);
	void OnHoldReleased(const FVector2D& Screenposition, float Downtime);
	void OnSwipeStarted(const FVector2D& Screenposition, float Downtime);
	void OnSwipeUpdate(const FVector2D& Screenposition, float Downtime);
	void OnSwipeReleased(const FVector2D& Screenposition, float Downtime);
	void OnSwipeTwoPointsStarted(const FVector2D& ScreenPosition1, const FVector2D& ScreenPosition2, float DownTime);
	void OnSwipeTwoPointsUpdate(const FVector2D& ScreenPosition1, const FVector2D& ScreenPosition2, float DownTime);
	void OnPinchStarted(const FVector2D& AnchorPosition1, const FVector2D& AnchorPosition2, float DownTime);
	void OnPinchUpdate(const FVector2D& ScreenPosition1, const FVector2D& ScreenPosition2, float DownTime);

	/** Toggles the ingame menu display. */
	void OnToggleInGameMenu();

protected:
	/** if set, input and camera updates will be ignored */
	uint8 bIgnoreInput : 1;

	/** currently selected actor */
	TWeakObjectPtr<AActor> SelectedActor;

	/** Swipe anchor. */
	FVector SwipeAnchor3D;

	FVector2D PrevSwipeScreenPosition;

	/** Previous swipe mid point. */
	FVector2D PrevSwipeMidPoint;

	/** Custom input handler. */
	UPROPERTY()
	class UAboveBelowInput* InputHandler;

	/**
	* Change current selection (on toggle on the same).
	*
	* @param	NewFocus	Actor to focus on.
	* @param	NewPosition
	*/
	void SetSelectedActor(AActor* NewFocus, const FVector& NewPosition);

	/**
	* Get friendly target under screen space coordinates.
	*
	* @param	ScreenPoint	Screen coordinates to check
	* @param	WorldPoint	Point in the world the screen coordinates projected onto.
	*/
	AActor* GetFriendlyTarget(const FVector2D& ScreenPoint, FVector& WorldPoint) const;

	/**
	* Get audio listener position and orientation.
	*
	* @param
	* @param
	* @param
	*/
	virtual void GetAudioListenerPosition(FVector& Location, FVector& FrontDir, FVector& RightDir) override;

private:

	/** Helper to return camera component via spectator pawn. */
	UAboveBelowCameraComponent* GetCameraComponent() const;
	
};
