// Fill out your copyright notice in the Description page of Project Settings.

#include "AboveBelow.h"
#include "PlayerBlocks.h"


// Sets default values
APlayerBlocks::APlayerBlocks()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void APlayerBlocks::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APlayerBlocks::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

