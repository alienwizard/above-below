// Fill out your copyright notice in the Description page of Project Settings.
//Base class for all powerups

#include "AboveBelow.h"
#include "powerup.h"


// Sets default values
Apowerup::Apowerup()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void Apowerup::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void Apowerup::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

