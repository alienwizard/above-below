// Fill out your copyright notice in the Description page of Project Settings.

#include "AboveBelow.h"
#include "points.h"


// Sets default values
Apoints::Apoints()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void Apoints::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void Apoints::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

