// Fill out your copyright notice in the Description page of Project Settings.

#include "AboveBelow.h"
#include "AboveBelowGameMode.h"
#include "MyPlayerController.h"
#include "MyPlayer.h"
#include "Engine.h"

AAboveBelowGameMode::AAboveBelowGameMode(const FObjectInitializer& ObjectInitializer)
: Super(ObjectInitializer)
{
	// use the custom controller
	PlayerControllerClass = AMyPlayerController::StaticClass();
}

void AAboveBelowGameMode::StartPlay()
{
	Super::StartPlay();

	StartMatch();

	if (GEngine)
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("HELLO"));
	}
}



